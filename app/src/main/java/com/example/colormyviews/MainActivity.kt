package com.example.colormyviews

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.colormyviews.R.color
import com.example.colormyviews.R.id
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setListeners()
    }

    private fun setListeners() {
        val clickableViews = listOf(
            textBoxOne,
            textBoxTwo,
            textBoxThree,
            textBoxFour,
            textBoxFive,
            btnRed,
            btnYellow,
            btnGreen
        )

        for (item in clickableViews) {
            item.setOnClickListener { makeColored(it) }
        }
    }

    private fun makeColored(view: View) {
        when (view.id) {
            // Boxes using Color class colors for background
            id.textBoxOne -> view.setBackgroundColor(Color.DKGRAY)
            id.textBoxTwo -> view.setBackgroundColor(Color.GRAY)

            // Boxes using Android color resources for background
            id.textBoxThree -> view.setBackgroundResource(android.R.color.holo_green_light)
            id.textBoxFour -> view.setBackgroundResource(android.R.color.holo_green_dark)
            id.textBoxFive -> view.setBackgroundResource(android.R.color.holo_green_light)
            id.btnRed -> textBoxThree.setBackgroundResource(color.mRed)
            id.btnYellow -> textBoxFour.setBackgroundResource(color.mYellow)
            id.btnGreen -> textBoxFive.setBackgroundResource(color.mGreen)

            else -> view.setBackgroundColor(Color.LTGRAY)

        }
    }
}
